(function ($) {
    $(document).ready(function () {
        var dropdown = $('#vendorSupplyType');  // Change this ID to match the actual ID of your dropdown
        var textbox = $('#vendorFSSAINo');  // Change this ID to match the actual ID of your textbox

        // Initial state
        if (dropdown.val() === '1') {
            textbox.parent().show();  // Show the textbox container
        } else {
            textbox.parent().hide();  // Hide the textbox container
        }

        // Event listener for dropdown change
        dropdown.change(function () {
            if (dropdown.val() === '1') {
                textbox.parent().show();  // Show the textbox container
            } else {
                textbox.parent().hide();  // Hide the textbox container
            }
        });
    });
})(django.jQuery);