from django.db import models
import uuid
from django.utils import timezone
from Authentication.models import User

# Create your models here.
taxType = ((1, 'Percentage'),
           (2, 'Flat'))


class BaseModel(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    created_at = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="%(class)s_created_by")
    updated_at = models.DateTimeField(default=timezone.now)
    updated_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="%(class)s_updated_by")
    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    class Meta:
        abstract = True


class Tax(BaseModel):
    taxName = models.CharField(max_length=100)
    taxType = models.IntegerField(choices=taxType, default=1)
    taxValue = models.FloatField(default=0.00)

    def __str__(self):
        return self.taxName


class MeasurementUnit(BaseModel):
    measurementUnitName = models.CharField(max_length=100)
    measurementUnitShortName = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.measurementUnitName} -  {self.measurementUnitShortName}"


class Packaging(BaseModel):
    nameOfPack = models.CharField(max_length=100)
    measurementType = models.ForeignKey('MeasurementUnit', on_delete=models.SET_NULL, null=True,
                                        related_name="MeasurementUnit")

    def __str__(self):
        return self.nameOfPack
