from django.shortcuts import render
from .models import *
from .serializers import TaxSerializer, PackagingSerializer, MeasurementUnitSerializer
from rest_framework import viewsets
from django.http import JsonResponse
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication


# Create your views here.


class TaxView(viewsets.ModelViewSet):
    queryset = Tax.objects.all()
    serializer_class = TaxSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.queryset.filter(created_by=self.request.user, is_deleted=False)

    def create(self, request, *args, **kwargs):
        request.data['created_by'] = request.user.pk
        return super(TaxView, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        request.data['updated_by'] = request.user.pk
        return super(TaxView, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.updated_by = request.user
        instance.is_deleted = True
        instance.save()
        res = {'msg': 'Records Deleted Successfully'}
        return JsonResponse(res, safe=False)


class PackagingView(viewsets.ModelViewSet):
    queryset = Packaging.objects.all()
    serializer_class = PackagingSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.queryset.filter(created_by=self.request.user, is_deleted=False)

    def create(self, request, *args, **kwargs):
        request.data['created_by'] = request.user.pk
        return super(PackagingView, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        request.data['updated_by'] = request.user.pk
        return super(PackagingView, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.updated_by = request.user
        instance.is_deleted = True
        instance.save()
        res = {'msg': 'Records Deleted Successfully'}
        return JsonResponse(res, safe=False)


class MeasurementUnitView(viewsets.ModelViewSet):
    queryset = MeasurementUnit.objects.all()
    serializer_class = MeasurementUnitSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.queryset.filter(created_by=self.request.user, is_deleted=False)

    def create(self, request, *args, **kwargs):
        request.data['created_by'] = request.user.pk
        return super(MeasurementUnitView, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        request.data['updated_by'] = request.user.pk
        return super(MeasurementUnitView, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.updated_by = request.user
        instance.is_deleted = True
        instance.save()
        res = {'msg': 'Records Deleted Successfully'}
        return JsonResponse(res, safe=False)
