from django.contrib import admin
from .models import *


# Register your models here.
@admin.register(Tax)
class TaxAdmin(admin.ModelAdmin):
    list_display = ['id', 'taxName', 'taxType', 'taxValue', 'created_at', 'created_by', 'updated_at', 'updated_by',
                    'is_deleted', 'is_active']


@admin.register(Packaging)
class PackagingAdmin(admin.ModelAdmin):
    list_display = ['id', 'nameOfPack', 'measurementType', 'created_at', 'created_by', 'updated_at', 'updated_by',
                    'is_deleted', 'is_active']


@admin.register(MeasurementUnit)
class MeasurementUnitAdmin(admin.ModelAdmin):
    list_display = ['id', 'measurementUnitName', 'measurementUnitShortName', 'created_at', 'created_by', 'updated_at',
                    'updated_by', 'is_deleted', 'is_active']
