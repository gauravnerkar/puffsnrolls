from rest_framework import serializers
from .models import *


class TaxSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tax
        fields = ['id', 'taxName', 'taxType', 'taxValue', 'created_at', 'created_by', 'updated_at', 'updated_by',
                  'is_deleted', 'is_active']


class PackagingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Packaging
        fields = ['id', 'nameOfPack', 'measurementType', 'created_at', 'created_by', 'updated_at', 'updated_by',
                  'is_deleted', 'is_active']


class MeasurementUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeasurementUnit
        fields = ['id', 'measurementUnitName', 'measurementUnitShortName', 'created_at', 'created_by', 'updated_at',
                  'updated_by',
                  'is_deleted', 'is_active']
