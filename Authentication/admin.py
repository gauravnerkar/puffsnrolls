from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext, gettext_lazy as _
from .models import *


@admin.register(User)
class UserPanel(UserAdmin):
    model = User
    change_user_password_template = None
    fieldsets = (
        (None, {'fields': ('profile', 'email', 'password')}),
        (_('Personal info'), {'fields': (
            'first_name', 'last_name', 'mobile', 'created_by')}),
        (_('User Type'), {
            'fields': (
                'user_type', 'sub_user_type'),
        }),
        (_('Activation Status'), {
            'fields': (
                'is_active', 'is_phone_verified', 'is_email_verified'),
        }),
        (_('Permissions'), {
            'fields': ('is_staff', 'is_superuser',
                       'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'profile', 'full_name', 'mobile', 'sub_user_type', 'is_staff',)
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('first_name', 'last_name', 'email', 'mobile')
    ordering = ('date_joined',)
    required = ('first_name', 'last_name')
    filter_horizontal = ('groups', 'user_permissions',)
