from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.models import auth
from .models import *
from django.contrib.auth import get_user_model, authenticate
from django.views.generic.base import View
from .form import *
from django.views.generic.base import TemplateView
from django.conf import settings
from django.contrib.auth import views as auth_views
from django.utils.translation import gettext_lazy as _
from django.template.loader import get_template
import string
import random
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.viewsets import ModelViewSet
# from puffsnrolls.JwtAuthentication import JwtCustomAuthentication
from rest_framework.permissions import IsAuthenticated
from .Serializers import *
from rest_framework.exceptions import *
from .helper import *
from puffsnrolls.helper import GenerateOTP
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.pagination import LimitOffsetPagination
from django.db.models import Value
from django.db.models.functions import Concat
# from django.db.transaction import atomic
from django.db import transaction
from django.contrib.contenttypes.models import ContentType
from rest_framework_simplejwt.authentication import JWTAuthentication


def randomPasswordGenerator():
    characters = string.ascii_letters + string.digits + string.punctuation
    password = ''.join(random.choice(characters) for i in range(8))
    return password


class SignupAPI(APIView):

    @transaction.atomic
    def post(self, request):
        fm = UserRegistrationSerializer(data=request.data)
        if fm.is_valid():
            fm.save()
            return Response({"message": "One time password has sent on Mobile"},
                            status=HTTP_201_CREATED)
        else:
            return Response(fm.errors, status=HTTP_400_BAD_REQUEST)


class ObtainOTPForUser(APIView):

    @transaction.atomic
    def post(self, request):
        sender = None
        attrs = request.data
        userModel = get_user_model()
        context = {}
        try:
            user_modal = userModel.objects.get(mobile=attrs.get('mobile'))
            if not user_modal.is_phone_verified:
                otp = GenerateOTP()
                user_modal.phone_otp = otp
                user_modal.expiry_phone_otp = setExpiryDateTimeForOTP()
                user_modal.save()
                send_otp_twilio(otp, user_modal.mobile)
                context['phone_no'] = user_modal.mobile
                context['phone'] = "phone not verified otp sent on phone"
                raise ValidationError(context)
        except userModel.DoesNotExist:
            raise ValidationError({"mobile": "Invalid mobile number"})
        # try:
        #     token = authenticate(**attrs)
        #     if not token:
        #         raise ValidationError({'password': 'Incorrect password'})
        # except Exception as e:
        #     raise ValidationError({'password': 'Incorrect password'})
        code = GenerateOTP()
        user_modal.phone_otp = code
        user_modal.expiry_phone_otp = setExpiryDateTimeForOTP()
        user_modal.save()
        send_otp_twilio(code, user_modal.mobile)
        # user hashing
        message = user_message(user_modal)
        return Response(
            {'token': f"{message['uid']}/{message['token']}", 'otp': code},
            status=HTTP_200_OK)


class resendOTPOnmessage(APIView):

    @transaction.atomic
    def post(self, request):
        body = request.data
        try:
            user = User.objects.get(mobile=body['mobile'])
            otp = self.getOTP(user)
            if not user.is_phone_verified:
                send_otp_by_plivo(otp, user.mobile)

        except User.DoesNotExist:
            error_name = "Invalid User"
            error_message = "User doesnot exist"
            raise AuthenticationFailed(error_message, error_name)
        return HttpResponse("Accepted", status=HTTP_202_ACCEPTED)

    @staticmethod
    def getOTP(user):
        res_message = random.randint(100000, 999999)
        otp = OneTimePassword(user=user,
                              one_time_password_for_mobile=res_message)
        otp.save()

        return {"message": res_message}


class resendOTPOncall(APIView):

    @transaction.atomic
    def post(self, request):
        body = request.data
        try:
            user = User.objects.get(mobile=body['mobile'])
            otp = self.getOTP(user)
            if not user.is_phone_verified:
                call_for_otp_by_twilio(otp, user.mobile)

        except User.DoesNotExist:
            error_name = "Invalid User"
            error_call = "User doesnot exist"
            raise AuthenticationFailed(error_call, error_name)
        return HttpResponse("Accepted", status=HTTP_202_ACCEPTED)

    @staticmethod
    def getOTP(user):
        res_call = random.randint(100000, 999999)
        otp = OneTimePassword(user=user,
                              one_time_password_for_mobile=res_call)
        otp.save()

        return {"call": res_call}


class ChangeMobileNo(APIView):

    def post(self, request):
        body = request.data
        try:
            user = User.objects.get(mobile=body['new_mobile'])
            if user:
                error_name = "mobile"
                error_message = "User already exist with new Mobile Number"
                raise ValidationError({error_name: error_message})
        except User.DoesNotExist:
            pass
        try:
            user = User.objects.get(mobile=body['old_mobile'])
            user.mobile = body['new_mobile']
            user.save()
            otp = self.getOTP(user)

            if not user.is_phone_verified:
                send_otp_by_plivo(otp, user.mobile)

        except User.DoesNotExist:
            error_name = "Invalid User"
            error_message = "User doesnot exist"
            raise ValidationError({error_name: error_message})
        except Exception as e:
            raise e
        return Response({"message": "change"}, status=HTTP_202_ACCEPTED)

    @staticmethod
    def getOTP(user):
        res_call = random.randint(100000, 999999)
        otp = OneTimePassword(user=user,
                              one_time_password_for_mobile=res_call)
        otp.save()

        return {"call": res_call}


class ChangeEmail(APIView):

    @transaction.atomic
    def post(self, request):
        body = request.data
        try:
            user = User.objects.get(email=body['old_email'])
            user.email = body['new_email']
            user.save()
            otp = self.getOTP(user)

            if not user.is_email_verified:
                send_email_otp(user.full_name, otp, user.email)

        except User.DoesNotExist:
            error_name = "Invalid User"
            error_message = "User doesnot exist"
            raise AuthenticationFailed(error_message, error_name)
        return Response({"message": "Email has changed"}, status=HTTP_202_ACCEPTED)

    @staticmethod
    def getOTP(user):
        res_email = random.randint(100000, 999999)
        otp = OneTimePassword(user=user,
                              one_time_password_for_email=res_email)
        otp.save()

        return {"email": res_email}


class resendOTPOnEmail(APIView):

    def post(self, request):
        body = request.data
        try:
            user = User.objects.get(email=body['email'])
            otp = random.randint(100000, 999999)
            user.email_otp = otp
            user.expiry_email_otp = setExpiryDateTimeForOTP()
            user.save()
            if not user.is_email_verified:
                send_email_otp(user.full_name, otp, user.email)

        except User.DoesNotExist:
            error_name = "Invalid User"
            error_message = "User doesnot exist"
            raise AuthenticationFailed(error_message, error_name)
        return Response({"email": body['email']}, status=HTTP_202_ACCEPTED)


class SendOTPView(APIView):

    @transaction.atomic
    def post(self, request, uidb64, token):
        User = get_user_model()
        attempt = request.data['attempt']
        try:
            uid = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and generate_token.check_token(user, token):
            try:
                user.is_email_verified = True
                user.save()
                otp = GenerateOTP()
                if attempt == 1:
                    send_otp_twilio(otp, user.mobile)
                elif attempt == 2:
                    send_otp_by_plivo(otp, user.mobile)
                elif attempt == 3:
                    call_for_otp_by_twilio(otp, user.mobile)
                message = user_message(user)
                return Response({"message": "otp has sent on your mobile number"}, status=HTTP_202_ACCEPTED)

            except User.DoesNotExist:
                error_name = "Invalid User"
                error_password = "User doesnot exist"
                raise AuthenticationFailed(error_password, error_name)


class VerifyMobile(APIView):

    @transaction.atomic
    def post(self, request):
        body = request.data
        try:
            user = User.objects.get(mobile=body['mobile'])
            if user.phone_otp == int(body['otp']):
                if user.expiry_phone_otp <= timezone.now():
                    error_name = "Invalid OTP"
                    error_message = "One Time Password has expired"
                    raise AuthenticationFailed({error_name: error_message})
                user.is_phone_verified = True
                user.is_active = True
                user.save()
            else:
                error_name = "Invalid OTP"
                error_message = "OTP is either invalid or expired"
                raise AuthenticationFailed({error_name: error_message})
        except User.DoesNotExist:
            error_name = "Invalid User"
            error_message = "User doesnot exist"
            raise AuthenticationFailed({error_name: error_message})
        return HttpResponse("Accepted", status=HTTP_202_ACCEPTED)


class VerifyEmail(APIView):

    @transaction.atomic
    def post(self, request):
        body = request.data
        try:
            user = User.objects.get(email=body['email'])
            if user.email_otp == int(body['otp']):
                if user.expiry_email_otp <= timezone.now():
                    error_name = "Invalid OTP"
                    error_message = "One Time Password has expired"
                    raise AuthenticationFailed(error_message, error_name)
                user.is_email_verified = True
                user.save()
                if user.is_phone_verified and user.is_email_verified:
                    user.is_active = True
                    user.save()
            else:
                error_name = "Invalid OTP"
                error_message = "OTP is either invalid or expired"
                raise exceptions.AuthenticationFailed(error_message, error_name)
        except User.DoesNotExist:
            error_name = "Invalid User"
            error_message = "User doesnot exist"
            raise exceptions.AuthenticationFailed(error_message, error_name)
        return Response({"message": "Accepted"}, status=HTTP_202_ACCEPTED)


class LogInPage(TemplateView):
    template_name = 'authentication/auth_login.html'

    def get(self, request):
        user_form = User_Form()
        return render(request, self.template_name, {"form": user_form})

    @transaction.atomic
    def post(self, request):
        user_form = User_Form(data=request.POST)
        if user_form.is_valid():
            user = User.objects.filter(email=user_form.cleaned_data['email'])
            if user:
                for i in user:
                    if i.is_active is False:
                        mail_subject = 'Activate your account.'
                        message = {
                            'user': i,
                            'domain': get_current_site(request),
                            'uid': urlsafe_base64_encode(force_bytes(i.pk)),
                            'token': generate_token.make_token(i),
                        }
                        plain_text = get_template('Email/sign_up/email_template.txt').render(message)
                        htmly = get_template('Email/sign_up/email_template.html').render(message)

                        msg = EmailMultiAlternatives(mail_subject, plain_text, settings.EMAIL_HOST_USER,
                                                     [user_form.cleaned_data['email']])
                        msg.attach_alternative(htmly, "text/html")
                        msg.send()
                        messages.info(request,
                                      "Account Exists but Email is not verified.... Verification link has sent to Email")
                        return render(request, self.template_name, {"form": user_form})
            user = auth.authenticate(email=user_form.cleaned_data['email'], password=user_form.cleaned_data['password'])
            if user is not None:
                auth.login(request, user)
                if request.GET.get('next'):
                    return redirect(request.GET["next"])
                response = redirect("/")
                return response
            else:
                messages.info(request, "Invalid Credential !")
                response = render(request, self.template_name, {"form": user_form})
            return response
        else:
            messages.info(request, user_form.errors)
            return render(request, self.template_name, {"form": user_form})


def LogOut(request):
    auth.logout(request)
    return redirect("/")


class Login(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer


def activate(request, uidb64, token):
    User = get_user_model()
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(id=request.user.id)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and generate_token.check_token(user, token):
        user.is_active = True
        user.save()
        return redirect('login')
    else:
        return HttpResponse('Activation link is invalid!')


class ResetPassword(APIView):

    @transaction.atomic
    def post(self, request):
        body = request.data
        try:
            user = User.objects.get(email=body['email'])
            if not user.is_active:
                raise AuthenticationFailed("Please activate your account", "User activation")
            message = user_message(user)
            send_email(message, 'Reset Password you account', user.email, 'Email/ResetPassword'
                                                                          '/resetpassword.html',
                       'Email/ResetPassword/resetpassword.txt')
            return Response({"message": "Reset link has sent on your Email"}, status=HTTP_202_ACCEPTED)

        except User.DoesNotExist:
            error_name = "Invalid User"
            error_password = "User doesnot exist"
            raise AuthenticationFailed(error_password, error_name)


class NewPasswordSetup(APIView):

    @transaction.atomic
    def post(self, request, uidb64, token):
        password = request.data['password']
        confirm_password = request.data['confirm_password']
        User = get_user_model()
        try:
            uid = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and generate_token.check_token(user, token):
            try:
                if password == confirm_password:
                    user.set_password(password)
                    user.save()
                    messages = {
                        'key': 'your password successfully reset'
                    }

                    return Response(messages, status=HTTP_202_ACCEPTED)

            except OneTimePassword.DoesNotExist:
                error_name = "Invalid OTP"
                error_message = "OTP is either invalid or expired"
                raise AuthenticationFailed(error_message, error_name)
            return redirect('login')
        else:
            raise ValidationError({"link": "Activation url has expired"})


class PasswordResetLink(auth_views.PasswordResetView):
    template_name = 'authentication/auth_recover.html'
    title = _('Password reset sent')
    extra_context = {"info": "Please check you email if not present check promotion or spam"}


class UserCreationApi(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = AllCreateUserSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_field = 'uid'

    def create(self, request, *args, **kwargs):
        request_user = request.user
        if request_user.user_type in [1, 2] and request.data['user_type'] == 2:
            if request_user.user_type in [1, 2] and request.data['user_type'] == 2:
                request.data['created_by'] = request_user.pk
                instance = super(UserCreationApi, self).create(request, *args, **kwargs)
            if request_user.user_type == 2:
                request.data['created_by'] = request_user.pk
                instance = super(UserCreationApi, self).create(request, *args, **kwargs)
                return Response({"message": "User Created"}, status=HTTP_201_CREATED)
        elif request_user.user_type == 1:
            request.data['created_by'] = request_user.pk
            instance = super(UserCreationApi, self).create(request, *args, **kwargs)
            return Response({"message": "User Created"}, status=HTTP_201_CREATED)
        raise ValidationError("You are not authorised to create user")


class CreateUserApi(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = CreateUserSerializer
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_field = 'uid'

    def create(self, request, *args, **kwargs):
        request_user = request.user
        if request_user.user_type in [1, 2]:
            if request_user.user_type == 2:
                request.data['created_by'] = request_user.pk
                request.data['user_type'] = request_user.user_type
                request.data['sub_user_type'] = 2
            if request_user.user_type == 3:
                request.data['created_by'] = request_user.pk
                request.data['user_type'] = request_user.user_type
                request.data['sub_user_type'] = 1
            instance = super(CreateUserApi, self).create(request, *args, **kwargs)
            return Response({"message": "User Successfully Created"}, status=HTTP_201_CREATED)
        raise PermissionDenied({"message": "You are not authorized to create user"})
