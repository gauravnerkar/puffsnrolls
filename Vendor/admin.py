from django.contrib import admin
from .models import *


# Register your models here.

@admin.register(Vendor)
class VendorAdmin(admin.ModelAdmin):
    list_display = ['id', 'companyName', 'vendorSupplyType', 'contactPersonName', 'phoneNo', 'email', 'created_at',
                    'created_by', 'updated_at', 'updated_by',
                    'is_deleted', 'is_active']

    class Media:
        js = ('admin/js/admin_custom.js',)
