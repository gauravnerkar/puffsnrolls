from rest_framework import serializers
from .models import *


class VendorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vendor
        fields = ['id', 'companyName', 'vendorSupplyType', 'contactPersonName', 'phoneNo', 'email', 'created_at', 'created_by', 'updated_at', 'updated_by',
                  'is_deleted', 'is_active']
