from django.db import models
import uuid
from django.utils import timezone
from Authentication.models import User

supplyType = ((0, '-------'), (1, 'Food'), (2, 'Non Food'))

accountType = ((0, '-------'), (1, 'Saving Account'), (2, 'Current Account'))


# Create your models here.
class BaseModel(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    created_at = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="%(class)s_created_by")
    updated_at = models.DateTimeField(default=timezone.now)
    updated_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="%(class)s_updated_by")
    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    class Meta:
        abstract = True


class Vendor(BaseModel):
    companyName = models.CharField(max_length=200)
    vendorSupplyType = models.IntegerField(choices=supplyType, help_text="Vendor Supply Type", default=0)
    contactPersonName = models.CharField(max_length=200)
    vendorFSSAINo = models.CharField(verbose_name='vendorFSSAINo', max_length=20, blank=True)
    phoneNo = models.CharField(verbose_name='Phone No', max_length=20, unique=True)
    email = models.CharField(verbose_name='Email', max_length=200, unique=True, default='')
    address = models.TextField(help_text='Address', blank=False, null=True)
    gstNumber = models.CharField(verbose_name='GST Number', unique=True)
    bankName = models.CharField(verbose_name='Name Of Bank', null=True, blank=True)
    accNo = models.CharField(verbose_name='Account Number', null=True, blank=True)
    typeOfAcc = models.IntegerField(choices=accountType, default=0, help_text="Account Type")
    ifscCode = models.CharField(verbose_name='IFSC Code', unique=True, null=True, blank=True)
    accHolderName = models.CharField(verbose_name='Account Holder Name', null=True, blank=True)
    upiID = models.CharField(verbose_name='UPI Code', null=True, blank=True)
