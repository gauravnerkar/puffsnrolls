from django.db import models
import uuid
from django.utils import timezone
from Authentication.models import User
from MasterData.models import Tax, Packaging


# Create your models here.


class BaseModel(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    created_at = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="%(class)s_created_by")
    updated_at = models.DateTimeField(default=timezone.now)
    updated_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="%(class)s_updated_by")
    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    class Meta:
        abstract = True


class Category(BaseModel):
    categoryTitle = models.CharField(max_length=100)

    def __str__(self):
        return self.categoryTitle


class Product(BaseModel):
    productName = models.CharField(max_length=200)
    productImage = models.ImageField(upload_to='uploads/product/')
    productCategory = models.ForeignKey('Category', on_delete=models.SET_NULL, null=True,
                                        related_name='product_category')
    productTax = models.ForeignKey(Tax, on_delete=models.SET_NULL, null=True, related_name='product_tax')
    productPackaging = models.ForeignKey(Packaging, on_delete=models.SET_NULL, null=True, related_name='product_packaging')
    productMRP = models.FloatField(max_length=10)
