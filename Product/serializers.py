from rest_framework import serializers
from .models import *


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'categoryTitle', 'created_at', 'created_by', 'updated_at', 'updated_by',
                  'is_deleted', 'is_active']


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'productName', 'productImage', 'productCategory', 'productTax', 'productPackaging',
                  'productMRP', 'created_at', 'created_by', 'updated_at', 'updated_by',
                  'is_deleted', 'is_active']
