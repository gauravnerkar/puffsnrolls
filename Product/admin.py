from django.contrib import admin
from .models import *


# Register your models here.

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'categoryTitle', 'created_at', 'created_by', 'updated_at', 'updated_by',
                    'is_deleted', 'is_active']


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['id', 'productName', 'productImage', 'productCategory', 'productTax', 'productPackaging',
                    'productMRP', 'created_at', 'created_by', 'updated_at', 'updated_by',
                    'is_deleted', 'is_active']
