"""
URL configuration for puffsnrolls project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from Authentication.views import *
from MasterData.views import TaxView, PackagingView, MeasurementUnitView
from Product.views import CategoryView, ProductView
from Chef.views import ChefView
from Vendor.views import VendorView

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('tax', TaxView, basename='tax api')
router.register('packaging', PackagingView, basename='packaging api')
router.register('measurement_unit', MeasurementUnitView, basename='measurement unit api')
router.register('category', CategoryView, basename='category api')
router.register('product', ProductView, basename='product api')
router.register('chef', ChefView, basename='chef api')
router.register('vendor', VendorView, basename='vendor api')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('get_otp', ObtainOTPForUser.as_view(), name="Obtain OTP For User"),
    path('login', Login.as_view()),
    path('refreshToken', TokenRefreshView.as_view()),
    path('api/verify/', TokenVerifyView.as_view()),
    path('api/signup', SignupAPI.as_view(), name="signup"),
    path('api/phone_verified/', VerifyMobile.as_view(), name="Verified Mobile"),
    path('', include(router.urls)),
]
