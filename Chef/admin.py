from django.contrib import admin
from .models import *


# Register your models here.

@admin.register(ChefTicket)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'requirements', 'created_at', 'created_by', 'updated_at', 'updated_by',
                    'is_deleted', 'is_active']
