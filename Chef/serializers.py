from rest_framework import serializers
from .models import *


class ChefSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChefTicket
        fields = ['id', 'requirements', 'created_at', 'created_by', 'updated_at', 'updated_by',
                  'is_deleted', 'is_active']
