from django.db import models
import uuid
from django.utils import timezone
from Authentication.models import User


# Create your models here.

class BaseModel(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    created_at = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="%(class)s_created_by")
    updated_at = models.DateTimeField(default=timezone.now)
    updated_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="%(class)s_updated_by")
    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    class Meta:
        abstract = True


class ChefTicket(BaseModel):
    requirements = models.TextField(help_text="Reason", blank=False, null=True, default=0)

    def __str__(self):
        return self.requirements
